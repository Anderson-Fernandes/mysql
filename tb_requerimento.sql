create database db_requerimento_obra;

use db_requerimento_obra;

create table tb_obra
(
  cd_obra int not null,
  nm_obra varchar(60),
  dt_inicio date,
  dt_fim date,
  vl_orcamento decimal(9,2),
  constraint pk_obra primary key (cd_obra)
);

show tables;

desc tb_obra;

create table tb_fornecedor
(
  cd_fornecedor int not null,
  nm_fornecedor varchar(60),
  nm_endereco varchar(60),
  constraint pk_fornecedor primary key (cd_fornecedor)
);

show tables;

desc tb_fornecedor;

create table tb_material
(
  cd_material int not null,
  nm_material varchar(60),
  sg_unidade_medida char(2),
  constraint pk_material primary key (cd_material)
);

show tables;

desc tb_material;

create table tb_fornecimento
(
  cd_fornecimento int not null,
  qt_material decimal (9,2),
  cd_obra int,
  cd_fornecedor int,
  cd_material int,
  constraint pk_fornecimento primary key (cd_fornecimento),
  constraint fk_fornecimento_obra foreign key(cd_obra) references tb_obra(cd_obra),
  constraint fk_fornecimento_fornecedor foreign key(cd_fornecedor) references tb_fornecedor(cd_fornecedor),
  constraint fk_fornecedor_material foreign key (cd_material) references tb_material(cd_material)
);

show tables;

desc tb_fornecimento;
-- constraint fk_pedido_cliente foreign key(cd_cliente) references tb_cliente(cd_cliente),  -- on update 'cascade' / no 'action'/ 'set null'
