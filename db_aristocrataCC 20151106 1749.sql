-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_aristocratacc
--

CREATE DATABASE IF NOT EXISTS db_aristocratacc;
USE db_aristocratacc;

--
-- Definition of table `tb_bairro`
--

DROP TABLE IF EXISTS `tb_bairro`;
CREATE TABLE `tb_bairro` (
  `cd_bairro` int(11) NOT NULL,
  `nm_bairro` varchar(45) DEFAULT NULL,
  `cd_cidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_bairro`),
  KEY `fk_cidade` (`cd_cidade`),
  CONSTRAINT `fk_cidade` FOREIGN KEY (`cd_cidade`) REFERENCES `tb_cidade` (`cd_cidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bairro`
--

/*!40000 ALTER TABLE `tb_bairro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_bairro` ENABLE KEYS */;


--
-- Definition of table `tb_cidade`
--

DROP TABLE IF EXISTS `tb_cidade`;
CREATE TABLE `tb_cidade` (
  `cd_cidade` int(11) NOT NULL,
  `nm_cidade` varchar(20) DEFAULT NULL,
  `cd_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_cidade`),
  KEY `fk_estado` (`cd_estado`),
  CONSTRAINT `fk_estado` FOREIGN KEY (`cd_estado`) REFERENCES `tb_estado` (`cd_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cidade`
--

/*!40000 ALTER TABLE `tb_cidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cidade` ENABLE KEYS */;


--
-- Definition of table `tb_contato`
--

DROP TABLE IF EXISTS `tb_contato`;
CREATE TABLE `tb_contato` (
  `cd_contato` int(11) NOT NULL,
  `cd_telefone` varchar(12) DEFAULT NULL,
  `cd_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cd_contato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contato`
--

/*!40000 ALTER TABLE `tb_contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_contato` ENABLE KEYS */;


--
-- Definition of table `tb_dependente`
--

DROP TABLE IF EXISTS `tb_dependente`;
CREATE TABLE `tb_dependente` (
  `cd_dependente` int(11) NOT NULL,
  `nm_dependente` varchar(45) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `cd_socio` int(11) DEFAULT NULL,
  `cd_endereco` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_dependente`),
  KEY `fk_socio` (`cd_socio`),
  KEY `fk_enderecao` (`cd_endereco`),
  CONSTRAINT `fk_socio` FOREIGN KEY (`cd_socio`) REFERENCES `tb_socio` (`cd_socio`),
  CONSTRAINT `fk_enderecao` FOREIGN KEY (`cd_endereco`) REFERENCES `tb_endereco` (`cd_endereco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dependente`
--

/*!40000 ALTER TABLE `tb_dependente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_dependente` ENABLE KEYS */;


--
-- Definition of table `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
CREATE TABLE `tb_endereco` (
  `cd_endereco` int(11) NOT NULL,
  `nm_endereco` varchar(45) DEFAULT NULL,
  `cd_cep` char(8) DEFAULT NULL,
  `cd_bairro` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_endereco`),
  KEY `fk_bairro` (`cd_bairro`),
  CONSTRAINT `fk_bairro` FOREIGN KEY (`cd_bairro`) REFERENCES `tb_bairro` (`cd_bairro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_endereco`
--

/*!40000 ALTER TABLE `tb_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_endereco` ENABLE KEYS */;


--
-- Definition of table `tb_estado`
--

DROP TABLE IF EXISTS `tb_estado`;
CREATE TABLE `tb_estado` (
  `cd_estado` int(11) NOT NULL,
  `sg_uf` char(2) DEFAULT NULL,
  PRIMARY KEY (`cd_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_estado`
--

/*!40000 ALTER TABLE `tb_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_estado` ENABLE KEYS */;


--
-- Definition of table `tb_socio`
--

DROP TABLE IF EXISTS `tb_socio`;
CREATE TABLE `tb_socio` (
  `cd_socio` int(11) NOT NULL AUTO_INCREMENT,
  `nm_socio` varchar(45) DEFAULT NULL,
  `cd_cpf` char(11) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `cd_endereco` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_socio`),
  KEY `fk_endereco` (`cd_endereco`),
  CONSTRAINT `fk_endereco` FOREIGN KEY (`cd_endereco`) REFERENCES `tb_endereco` (`cd_endereco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_socio`
--

/*!40000 ALTER TABLE `tb_socio` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_socio` ENABLE KEYS */;


--
-- Definition of table `tb_vendedor`
--

DROP TABLE IF EXISTS `tb_vendedor`;
CREATE TABLE `tb_vendedor` (
  `cd_vendedor` int(11) NOT NULL,
  `vl_comissao` decimal(9,2) DEFAULT NULL,
  `ds_titulo` text,
  `nm_vendedor` varchar(45) DEFAULT NULL,
  `cd_cpf` char(11) DEFAULT NULL,
  PRIMARY KEY (`cd_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_vendedor`
--

/*!40000 ALTER TABLE `tb_vendedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_vendedor` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
