-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.21-MariaDB


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_requerimento_obra
--

CREATE DATABASE IF NOT EXISTS db_requerimento_obra;
USE db_requerimento_obra;

--
-- Definition of table `tb_fornecedor`
--

DROP TABLE IF EXISTS `tb_fornecedor`;
CREATE TABLE `tb_fornecedor` (
  `cd_fornecedor` int(11) NOT NULL,
  `nm_fornecedor` varchar(60) DEFAULT NULL,
  `nm_endereco` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`cd_fornecedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_fornecedor`
--

/*!40000 ALTER TABLE `tb_fornecedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_fornecedor` ENABLE KEYS */;


--
-- Definition of table `tb_fornecimento`
--

DROP TABLE IF EXISTS `tb_fornecimento`;
CREATE TABLE `tb_fornecimento` (
  `cd_fornecimento` int(11) NOT NULL,
  `qt_material` decimal(9,2) DEFAULT NULL,
  `cd_obra` int(11) DEFAULT NULL,
  `cd_fornecedor` int(11) DEFAULT NULL,
  `cd_material` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_fornecimento`),
  KEY `fk_fornecimento_obra` (`cd_obra`),
  KEY `fk_fornecimento_fornecedor` (`cd_fornecedor`),
  KEY `fk_fornecedor_material` (`cd_material`),
  CONSTRAINT `fk_fornecedor_material` FOREIGN KEY (`cd_material`) REFERENCES `tb_material` (`cd_material`),
  CONSTRAINT `fk_fornecimento_fornecedor` FOREIGN KEY (`cd_fornecedor`) REFERENCES `tb_fornecedor` (`cd_fornecedor`),
  CONSTRAINT `fk_fornecimento_obra` FOREIGN KEY (`cd_obra`) REFERENCES `tb_obra` (`cd_obra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_fornecimento`
--

/*!40000 ALTER TABLE `tb_fornecimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_fornecimento` ENABLE KEYS */;


--
-- Definition of table `tb_material`
--

DROP TABLE IF EXISTS `tb_material`;
CREATE TABLE `tb_material` (
  `cd_material` int(11) NOT NULL,
  `nm_material` varchar(60) DEFAULT NULL,
  `sg_unidade_medida` char(2) DEFAULT NULL,
  PRIMARY KEY (`cd_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_material`
--

/*!40000 ALTER TABLE `tb_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_material` ENABLE KEYS */;


--
-- Definition of table `tb_obra`
--

DROP TABLE IF EXISTS `tb_obra`;
CREATE TABLE `tb_obra` (
  `cd_obra` int(11) NOT NULL,
  `nm_obra` varchar(60) DEFAULT NULL,
  `dt_inicio` date DEFAULT NULL,
  `dt_fim` date DEFAULT NULL,
  `vl_orcamento` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`cd_obra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_obra`
--

/*!40000 ALTER TABLE `tb_obra` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_obra` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
