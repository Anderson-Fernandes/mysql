insert into tb_vendedor(nm_vendedor, vl_salario_fixo, cd_vendedor, sg_faixa_comissao) value ('Astolfo',6789.23,433,'B');

insert into tb_vendedor value(660, 'Antonomasia','C',5673.69);


insert into tb_cliente values
(720,'Ana','Rua 17 n. 19','Niteroi','24358310','RJ','12113231000134'),
(870,'Fl�vio','Av. Pres. Vargas, 10','S�o Paulo','22763931','SP','2253412693879'),
(110,'Jorge','Rua Caiapo,13','Curitiba','30078500','PR','1451226498349'),
(222,'Lucia','Rua Itabira 123 lj 9','Belo Horizonte','22124391','MG','2812525393488'),
(830,'Mauricio','Av. Paulista 1236 sl/ 2345','S�o Paulo','30126083','SP','32816.98574656'),
(130,'Edmar','Rua da Praia sn/','Salvador','30078900','BA','234632842349'),
(410,'Rodolfo','Largo da Lapa 27 Sobrado','Rio de Janeiro','30078900','RJ','1273657123469'),
(201,'Beth','Av. Clim�rio n.45','S�o Paulo','25679300','SP','3284822303242'),
(157,'Paulo','Tv. Morais c/3','Londrina','','PR','127.3655713474'),
(180,'Livio','Av Beira Mar n.1256','Florianopolis','30077500','SC','2176357102329'),
(260,'Susana','Rua Lopes Mendes, 12','Niteroi','30046500','RJ','1578985223657'),
(290,'Renato','Rua Meireles n. 123 bl2 sl 345','S�o Paulo','30225900','SP','7887414700019'),
(390,'Sebastio','Rua da Igreja n.10','Uberaba','30438700','MG','0212587401477'),
(234,'Jose','Quadra 3 bl. 3 sl. 1003','Bras�lia','22841650','DF','4887441212545');

insert into tb_vendedor values
(209,'Jose','C',1800.00),
(111,'Carlos','A',2490.00),
(011,'Jo�o','C',2780.00),
(240,'Antonio','C',9500.00),
(720,'Felipe','A',4600.00),
(213,'Jonas','A',2300.00),
(101,'Jo�o','C',2650.00),
(310,'Josias','B',870.00),
(250,'Maur�cio','B',2930.00);

insert into tb_produto values
(25,'Queijo','Kg',0.97),
(31,'Chocolate','BAR',0.87),
(78,'Vinho','L',2.00),
(22,'Linho','M',0.11),
(30,'A�ucar','SAC',0.30),
(53,'Linha','M',1.80),
(13,'Ouro','G',6.18),
(45,'Madeira','M',0.25),
(87,'Cano','M',1.97),
(77,'Papel','M',1.05);

insert into tb_pedido values
(121,'2012-02-01',20,410,209),
(97,'2012-02-01',20,720,101),
(101,'2012-03-07',15,720,101),
(137,'2012-06-06',20,720,720),
(148,'2012-06-06',20,720,101),
(189,'2012-06-06',15,870,213),
(104,'2012-07-09',30,110,101),
(203,'2012-07-09',30,830,250),
(98,'2011-12-12',20,410,209),
(143,'2011-12-13',30,201,111),
(105,'2011-12-13',15,180,240),
(111,'2012-02-01',20,260,240),
(103,'2012-02-01',20,260,11),
(91,'2012-03-07',20,260,11),
(138,'2012-06-06',20,260,11),
(108,'2012-06-06',108,290,310),
(119,'2012-03-07',119,390,250),
(172,'2012-03-07',127,410,11);

insert into item_pedido values
(121,25,10),
(121,31,35),
(97,77,20),
(101,31,9),
(101,78,18),
(101,13,5),
(98,77,5),
(148,45,8),
(148,31,7),
(148,77,3),
(148,25,10),
(148,78,30),
(104,53,32),
(203,31,6),
(189,78,45),
(143,31,20),
(143,78,10),
(105,78,10),
(111,25,10),
(111,78,70),
(103,53,37),
(91,77,40),
(138,22,10),
(138,77,35),
(138,53,18),
(108,13,17),
(119,77,40),
(119,13,6),
(119,22,10),
(119,53,43),
(137,13,8);



select * from tb_cliente;

select cd_cnpj as CNPJ, nm_cliente as Nome, nm_cidade as Cidade from tb_cliente;

select nm_vendedor as Vendedor, vl_salario_fixo as 'Salario atual', vl_salario_fixo * 2 as 'Salario dobrado' from tb_vendedor;

select acos(-1);

select asin(-1);

select pi() +0.000000000000000;

select sqrt(81);

select bin(123);

select hex(123);

select nm_cliente, length(nm_cliente) from tb_cliente;

select nm_cliente as Nome, concat(nm_cidade, ' - ', sg_uf) as Naturalidade from tb_cliente;

select nm_cidade as 'Nome da Cidade', sg_uf as UF from tb_cliente where nm_cidade <> 'Niteroi';