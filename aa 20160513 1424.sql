-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema estoque_basico
--

CREATE DATABASE IF NOT EXISTS estoque_basico;
USE estoque_basico;

--
-- Definition of table `entrada_produto`
--

DROP TABLE IF EXISTS `entrada_produto`;
CREATE TABLE `entrada_produto` (
  `cd_entrada_produto` int(11) NOT NULL AUTO_INCREMENT,
  `cd_produto` int(11) DEFAULT NULL,
  `qt_entrada` int(11) DEFAULT NULL,
  `vl_unitario` decimal(9,2) DEFAULT '0.00',
  `dt_entrada` date DEFAULT NULL,
  PRIMARY KEY (`cd_entrada_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entrada_produto`
--

/*!40000 ALTER TABLE `entrada_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrada_produto` ENABLE KEYS */;


--
-- Definition of trigger `tr_entradaproduto_is`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_entradaproduto_is`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_entradaproduto_is` AFTER INSERT ON `entrada_produto` FOR EACH ROW begin
      call sp_atualizaestoque (new.cd_produto, new.qt_entrada , new.vl_unitario);
end $$

DELIMITER ;

--
-- Definition of trigger `tr_entradaproduto_up`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_entradaproduto_up`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_entradaproduto_up` AFTER UPDATE ON `entrada_produto` FOR EACH ROW begin
      call sp_atualizaestoque (new.cd_produto, new.qt_entrada - old.qt_entrada , new.vl_unitario);
end $$

DELIMITER ;

--
-- Definition of table `estoque`
--

DROP TABLE IF EXISTS `estoque`;
CREATE TABLE `estoque` (
  `cd_estoque` int(11) NOT NULL AUTO_INCREMENT,
  `cd_produto` int(11) DEFAULT NULL,
  `qt_estoque` int(11) DEFAULT NULL,
  `vl_unitario` decimal(9,2) DEFAULT '0.00',
  PRIMARY KEY (`cd_estoque`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estoque`
--

/*!40000 ALTER TABLE `estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque` ENABLE KEYS */;


--
-- Definition of table `produto`
--

DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `cd_produto` int(11) NOT NULL AUTO_INCREMENT,
  `sg_status` char(1) NOT NULL DEFAULT 'a',
  `ds_produto` varchar(50) DEFAULT NULL,
  `qt_estoque_minimo` int(11) DEFAULT NULL,
  `qt_estoque_maximo` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto`
--

/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;


--
-- Definition of table `saida_produto`
--

DROP TABLE IF EXISTS `saida_produto`;
CREATE TABLE `saida_produto` (
  `cd_saida_produto` int(11) NOT NULL AUTO_INCREMENT,
  `cd_produto` int(11) DEFAULT NULL,
  `qt_saida` int(11) DEFAULT NULL,
  `dt_saida` date DEFAULT NULL,
  `vl_unitario` decimal(9,2) DEFAULT '0.00',
  PRIMARY KEY (`cd_saida_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saida_produto`
--

/*!40000 ALTER TABLE `saida_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `saida_produto` ENABLE KEYS */;


--
-- Definition of trigger `tr_saidaproduto_is`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_saidaproduto_is`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_saidaproduto_is` AFTER INSERT ON `saida_produto` FOR EACH ROW begin
      call sp_atualizaestoque (new.cd_produto, new.qt_saida -1, new.vl_unitario);
end $$

DELIMITER ;

--
-- Definition of trigger `tr_saidaproduto_up`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_saidaproduto_up`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_saidaproduto_up` AFTER UPDATE ON `saida_produto` FOR EACH ROW begin
      call sp_atualizaestoque (new.cd_produto, old.qt_saida - new.qt_saida, new.vl_unitario);
end $$

DELIMITER ;

--
-- Definition of trigger `tr_saidaproduto_dl`
--

DROP TRIGGER /*!50030 IF EXISTS */ `tr_saidaproduto_dl`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `tr_saidaproduto_dl` AFTER DELETE ON `saida_produto` FOR EACH ROW begin
      call sp_atualizaestoque (old.cd_produto, old.qt_saida, old.vl_unitario);
end $$

DELIMITER ;

--
-- Definition of procedure `sp_atualizaestoque`
--

DROP PROCEDURE IF EXISTS `sp_atualizaestoque`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_atualizaestoque`(id_prod int,qtde_comprada int, valor_unit decimal (9,2))
begin
  declare contador int(11);

  select count(*) into contador from estoque where cd_produto = id_prod;

  if contador > 0 then
    update estoque set qt_estoque=qt_estoque + qt_comprada,
    vl_unitario=valor_unit
    where cd_produto = id_prood;
  else
    insert into estoque (cd_produto, qt_estoque, vl_unitario)
      values (id_prod, qtde_comprada, valor_unit);
  end if;
end $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
