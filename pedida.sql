create database db_pedido;

use db_pedido;

-- Criando as tabelas do banco de dados

create table  tb_cliente
(
  cd_cliente int not null,-- auto_increment
  nm_cliente varchar(60),
  nm_endereco varchar(60),
  nm_cidade varchar(30),
  cd_cep char(8),
  sg_uf char(2),
  cd_cnpj char(14),
  constraint pk_cliente primary key (cd_cliente)
 );

show tables;
desc tb_cliente;

create table tb_vendedor
(
  cd_vendedor int not null,
  nm_vendedor varchar(60),
  sg_faixa_comissao char (1),
  vl_salario_fixo decimal(9,2),
  constraint pk_vendedor primary key (cd_vendedor)
);

show tables;
desc tb_vendedor;

create table tb_produto
(
  cd_produto int not null,
  nm_produto varchar(45),
  sg_produto varchar(3),
  vl_produto decimal (9,2),
  constraint pk_vendedor primary key (cd_produto)
);

show tables;
desc tb_produto;

create table tb_pedido
(
  cd_pedido int not null,
  dt_pedido date,
  qt_entrega tinyint,
  cd_cliente int,
  cd_vendedor int,
  constraint pk_pedido primary key (cd_pedido),
  constraint fk_pedido_cliente foreign key(cd_cliente) references tb_cliente(cd_cliente),  -- on update 'cascade' / no 'action'/ 'set null'
  constraint fk_pedido_vendedor foreign key(cd_vendedor) references tb_vendedor(cd_vendedor)
);

show tables;
desc tb_pedido;

create table item_pedido
(
  cd_pedido int,
  cd_produto int,
  qt_produto decimal(9,2),
  constraint ck_pedido_produto primary key (cd_pedido, cd_produto),
  constraint fk_item_pedido foreign key(cd_pedido) references tb_pedido(cd_pedido),
  constraint fk_item_produto foreign key(cd_produto) references tb_produto(cd_produto)
);

drop table tb_pedido;