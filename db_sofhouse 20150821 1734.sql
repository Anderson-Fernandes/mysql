-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_softhouse
--

CREATE DATABASE IF NOT EXISTS db_softhouse;
USE db_softhouse;

--
-- Definition of table `tb_dificuldade`
--

DROP TABLE IF EXISTS `tb_dificuldade`;
CREATE TABLE `tb_dificuldade` (
  `cd_dificuldade` int(11) NOT NULL,
  `qt_nivel_dificuldade` decimal(9,2) DEFAULT NULL,
  `nm_dificuldade` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cd_dificuldade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dificuldade`
--

/*!40000 ALTER TABLE `tb_dificuldade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_dificuldade` ENABLE KEYS */;


--
-- Definition of table `tb_nivel_programador`
--

DROP TABLE IF EXISTS `tb_nivel_programador`;
CREATE TABLE `tb_nivel_programador` (
  `cd_nivel_programador` int(11) NOT NULL,
  `nm_nivel_programador` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cd_nivel_programador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nivel_programador`
--

/*!40000 ALTER TABLE `tb_nivel_programador` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_nivel_programador` ENABLE KEYS */;


--
-- Definition of table `tb_programa`
--

DROP TABLE IF EXISTS `tb_programa`;
CREATE TABLE `tb_programa` (
  `cd_programa` int(11) NOT NULL,
  `nm_programa` varchar(45) DEFAULT NULL,
  `ds_programa` varchar(200) DEFAULT NULL,
  `ds_tempo_estimado` varchar(100) DEFAULT NULL,
  `cd_dificuldade` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_programa`),
  KEY `fk_programa_dificuldade` (`cd_dificuldade`),
  CONSTRAINT `fk_programa_dificuldade` FOREIGN KEY (`cd_dificuldade`) REFERENCES `tb_dificuldade` (`cd_dificuldade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_programa`
--

/*!40000 ALTER TABLE `tb_programa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_programa` ENABLE KEYS */;


--
-- Definition of table `tb_programador`
--

DROP TABLE IF EXISTS `tb_programador`;
CREATE TABLE `tb_programador` (
  `cd_programador` int(11) NOT NULL,
  `nm_programador` varchar(45) DEFAULT NULL,
  `cd_nivel_programador` int(11) DEFAULT NULL,
  `cd_setor` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_programador`),
  KEY `fk_programador_nivel_programador` (`cd_nivel_programador`),
  KEY `fk_programador_setor` (`cd_setor`),
  CONSTRAINT `fk_programador_nivel_programador` FOREIGN KEY (`cd_nivel_programador`) REFERENCES `tb_nivel_programador` (`cd_nivel_programador`),
  CONSTRAINT `fk_programador_setor` FOREIGN KEY (`cd_setor`) REFERENCES `tb_setor` (`cd_setor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_programador`
--

/*!40000 ALTER TABLE `tb_programador` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_programador` ENABLE KEYS */;


--
-- Definition of table `tb_programador_programa`
--

DROP TABLE IF EXISTS `tb_programador_programa`;
CREATE TABLE `tb_programador_programa` (
  `cd_programador` int(11) NOT NULL DEFAULT '0',
  `cd_programa` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cd_programador`,`cd_programa`),
  KEY `fk_item_programa` (`cd_programa`),
  CONSTRAINT `fk_item_programador` FOREIGN KEY (`cd_programador`) REFERENCES `tb_programador` (`cd_programador`),
  CONSTRAINT `fk_item_programa` FOREIGN KEY (`cd_programa`) REFERENCES `tb_programa` (`cd_programa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_programador_programa`
--

/*!40000 ALTER TABLE `tb_programador_programa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_programador_programa` ENABLE KEYS */;


--
-- Definition of table `tb_setor`
--

DROP TABLE IF EXISTS `tb_setor`;
CREATE TABLE `tb_setor` (
  `cd_setor` int(11) NOT NULL,
  `nm_setor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cd_setor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setor`
--

/*!40000 ALTER TABLE `tb_setor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_setor` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
