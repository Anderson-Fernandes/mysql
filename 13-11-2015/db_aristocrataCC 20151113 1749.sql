-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.21-MariaDB


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_aristocratacc
--

CREATE DATABASE IF NOT EXISTS db_aristocratacc;
USE db_aristocratacc;

--
-- Definition of table `socio_condicao`
--

DROP TABLE IF EXISTS `socio_condicao`;
CREATE TABLE `socio_condicao` (
  `cd_socio` int(11) NOT NULL DEFAULT '0',
  `cd_mensalidade` int(11) NOT NULL DEFAULT '0',
  `ds_condicao` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cd_socio`,`cd_mensalidade`),
  KEY `fk_socio_mensalidade` (`cd_mensalidade`),
  CONSTRAINT `fk_mensalidade_socio` FOREIGN KEY (`cd_socio`) REFERENCES `tb_socio` (`cd_socio`),
  CONSTRAINT `fk_socio_mensalidade` FOREIGN KEY (`cd_mensalidade`) REFERENCES `tb_mensalidade` (`cd_mensalidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `socio_condicao`
--

/*!40000 ALTER TABLE `socio_condicao` DISABLE KEYS */;
/*!40000 ALTER TABLE `socio_condicao` ENABLE KEYS */;


--
-- Definition of table `tb_bairro`
--

DROP TABLE IF EXISTS `tb_bairro`;
CREATE TABLE `tb_bairro` (
  `cd_bairro` int(11) NOT NULL,
  `nm_bairro` varchar(45) DEFAULT NULL,
  `cd_cidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_bairro`),
  KEY `fk_cidade_bairro` (`cd_cidade`),
  CONSTRAINT `fk_cidade_bairro` FOREIGN KEY (`cd_cidade`) REFERENCES `tb_cidade` (`cd_cidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bairro`
--

/*!40000 ALTER TABLE `tb_bairro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_bairro` ENABLE KEYS */;


--
-- Definition of table `tb_cidade`
--

DROP TABLE IF EXISTS `tb_cidade`;
CREATE TABLE `tb_cidade` (
  `cd_cidade` int(11) NOT NULL,
  `nm_cidade` varchar(20) DEFAULT NULL,
  `cd_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_cidade`),
  KEY `fk_estado_cidade` (`cd_estado`),
  CONSTRAINT `fk_estado_cidade` FOREIGN KEY (`cd_estado`) REFERENCES `tb_estado` (`cd_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cidade`
--

/*!40000 ALTER TABLE `tb_cidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cidade` ENABLE KEYS */;


--
-- Definition of table `tb_cobrador`
--

DROP TABLE IF EXISTS `tb_cobrador`;
CREATE TABLE `tb_cobrador` (
  `cd_cobrador` int(11) NOT NULL,
  `nm_cobrador` varchar(45) DEFAULT NULL,
  `ds_cobranca` text,
  PRIMARY KEY (`cd_cobrador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cobrador`
--

/*!40000 ALTER TABLE `tb_cobrador` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cobrador` ENABLE KEYS */;


--
-- Definition of table `tb_contato`
--

DROP TABLE IF EXISTS `tb_contato`;
CREATE TABLE `tb_contato` (
  `cd_contato` int(11) NOT NULL,
  `cd_telefone` varchar(12) DEFAULT NULL,
  `cd_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cd_contato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contato`
--

/*!40000 ALTER TABLE `tb_contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_contato` ENABLE KEYS */;


--
-- Definition of table `tb_dependente`
--

DROP TABLE IF EXISTS `tb_dependente`;
CREATE TABLE `tb_dependente` (
  `cd_depedente` int(11) NOT NULL,
  `nm_depedente` varchar(45) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `cd_socio` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_depedente`),
  KEY `fk_depedente_socio` (`cd_socio`),
  CONSTRAINT `fk_depedente_socio` FOREIGN KEY (`cd_socio`) REFERENCES `tb_socio` (`cd_socio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_dependente`
--

/*!40000 ALTER TABLE `tb_dependente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_dependente` ENABLE KEYS */;


--
-- Definition of table `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
CREATE TABLE `tb_endereco` (
  `cd_endereco` int(11) NOT NULL,
  `nm_endereco` varchar(45) DEFAULT NULL,
  `cd_cep` char(8) DEFAULT NULL,
  `cd_bairro` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_endereco`),
  KEY `fk_bairo_endereco` (`cd_bairro`),
  CONSTRAINT `fk_bairo_endereco` FOREIGN KEY (`cd_bairro`) REFERENCES `tb_bairro` (`cd_bairro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_endereco`
--

/*!40000 ALTER TABLE `tb_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_endereco` ENABLE KEYS */;


--
-- Definition of table `tb_estado`
--

DROP TABLE IF EXISTS `tb_estado`;
CREATE TABLE `tb_estado` (
  `cd_estado` int(11) NOT NULL,
  `sg_uf` char(2) DEFAULT NULL,
  PRIMARY KEY (`cd_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_estado`
--

/*!40000 ALTER TABLE `tb_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_estado` ENABLE KEYS */;


--
-- Definition of table `tb_funcionario`
--

DROP TABLE IF EXISTS `tb_funcionario`;
CREATE TABLE `tb_funcionario` (
  `cd_funcionario` int(11) NOT NULL,
  `nm_departamento` varchar(45) DEFAULT NULL,
  `nm_cargo` varchar(45) DEFAULT NULL,
  `vl_salario` decimal(9,2) DEFAULT NULL,
  `dt_admissao` date DEFAULT NULL,
  `cd_vendedor` int(11) DEFAULT NULL,
  `cd_cobrador` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_funcionario`),
  KEY `fk_funcionario_vendedor` (`cd_vendedor`),
  KEY `fk_funcionario_cobrador` (`cd_cobrador`),
  CONSTRAINT `fk_funcionario_cobrador` FOREIGN KEY (`cd_cobrador`) REFERENCES `tb_cobrador` (`cd_cobrador`),
  CONSTRAINT `fk_funcionario_vendedor` FOREIGN KEY (`cd_vendedor`) REFERENCES `tb_vendedor` (`cd_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_funcionario`
--

/*!40000 ALTER TABLE `tb_funcionario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_funcionario` ENABLE KEYS */;


--
-- Definition of table `tb_mensalidade`
--

DROP TABLE IF EXISTS `tb_mensalidade`;
CREATE TABLE `tb_mensalidade` (
  `cd_mensalidade` int(11) NOT NULL,
  `vl_mensalidade` decimal(9,2) DEFAULT NULL,
  `dt_vencimento` date DEFAULT NULL,
  `dt_emissao` date DEFAULT NULL,
  PRIMARY KEY (`cd_mensalidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mensalidade`
--

/*!40000 ALTER TABLE `tb_mensalidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_mensalidade` ENABLE KEYS */;


--
-- Definition of table `tb_socio`
--

DROP TABLE IF EXISTS `tb_socio`;
CREATE TABLE `tb_socio` (
  `cd_socio` int(11) NOT NULL,
  `nm_socio` varchar(45) DEFAULT NULL,
  `cd_cpf` char(11) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `dt_associacao` date DEFAULT NULL,
  `cd_endereco` int(11) DEFAULT NULL,
  `cd_contato` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_socio`),
  KEY `fk_endereco_socio` (`cd_endereco`),
  KEY `fk_endereco_contato` (`cd_contato`),
  CONSTRAINT `fk_endereco_contato` FOREIGN KEY (`cd_contato`) REFERENCES `tb_contato` (`cd_contato`),
  CONSTRAINT `fk_endereco_socio` FOREIGN KEY (`cd_endereco`) REFERENCES `tb_endereco` (`cd_endereco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_socio`
--

/*!40000 ALTER TABLE `tb_socio` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_socio` ENABLE KEYS */;


--
-- Definition of table `tb_vendedor`
--

DROP TABLE IF EXISTS `tb_vendedor`;
CREATE TABLE `tb_vendedor` (
  `cd_vendedor` int(11) NOT NULL DEFAULT '0',
  `vl_salario` decimal(9,2) DEFAULT NULL,
  `ds_titulo` text,
  `nm_vendedor` varchar(45) DEFAULT NULL,
  `cd_cpf` char(11) DEFAULT NULL,
  PRIMARY KEY (`cd_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_vendedor`
--

/*!40000 ALTER TABLE `tb_vendedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_vendedor` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
