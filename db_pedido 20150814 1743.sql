-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_pedido
--

CREATE DATABASE IF NOT EXISTS db_pedido;
USE db_pedido;

--
-- Definition of table `item_pedido`
--

DROP TABLE IF EXISTS `item_pedido`;
CREATE TABLE `item_pedido` (
  `cd_pedido` int(11) NOT NULL DEFAULT '0',
  `cd_produto` int(11) NOT NULL DEFAULT '0',
  `qt_produto` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`cd_pedido`,`cd_produto`),
  KEY `fk_item_produto` (`cd_produto`),
  CONSTRAINT `fk_item_pedido` FOREIGN KEY (`cd_pedido`) REFERENCES `tb_pedido` (`cd_pedido`),
  CONSTRAINT `fk_item_produto` FOREIGN KEY (`cd_produto`) REFERENCES `tb_produto` (`cd_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_pedido`
--

/*!40000 ALTER TABLE `item_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_pedido` ENABLE KEYS */;


--
-- Definition of table `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
CREATE TABLE `tb_cliente` (
  `cd_cliente` int(11) NOT NULL,
  `nm_cliente` varchar(60) DEFAULT NULL,
  `nm_endereco` varchar(60) DEFAULT NULL,
  `nm_cidade` varchar(30) DEFAULT NULL,
  `cd_cep` char(8) DEFAULT NULL,
  `sg_uf` char(2) DEFAULT NULL,
  `cd_cnpj` char(14) DEFAULT NULL,
  PRIMARY KEY (`cd_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_cliente`
--

/*!40000 ALTER TABLE `tb_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cliente` ENABLE KEYS */;


--
-- Definition of table `tb_pedido`
--

DROP TABLE IF EXISTS `tb_pedido`;
CREATE TABLE `tb_pedido` (
  `cd_pedido` int(11) NOT NULL,
  `dt_pedido` date DEFAULT NULL,
  `qt_entrega` tinyint(4) DEFAULT NULL,
  `cd_cliente` int(11) DEFAULT NULL,
  `cd_vendedor` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_pedido`),
  KEY `fk_pedido_cliente` (`cd_cliente`),
  KEY `fk_pedido_vendedor` (`cd_vendedor`),
  CONSTRAINT `fk_pedido_cliente` FOREIGN KEY (`cd_cliente`) REFERENCES `tb_cliente` (`cd_cliente`),
  CONSTRAINT `fk_pedido_vendedor` FOREIGN KEY (`cd_vendedor`) REFERENCES `tb_vendedor` (`cd_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pedido`
--

/*!40000 ALTER TABLE `tb_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_pedido` ENABLE KEYS */;


--
-- Definition of table `tb_produto`
--

DROP TABLE IF EXISTS `tb_produto`;
CREATE TABLE `tb_produto` (
  `cd_produto` int(11) NOT NULL,
  `nm_produto` varchar(40) DEFAULT NULL,
  `sg_produto` varchar(3) DEFAULT NULL,
  `vl_preco_unitario` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`cd_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produto`
--

/*!40000 ALTER TABLE `tb_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_produto` ENABLE KEYS */;


--
-- Definition of table `tb_vendedor`
--

DROP TABLE IF EXISTS `tb_vendedor`;
CREATE TABLE `tb_vendedor` (
  `cd_vendedor` int(11) NOT NULL,
  `nm_vendedor` varchar(60) DEFAULT NULL,
  `sg_faixa_comissao` char(1) DEFAULT NULL,
  `vl_salario_fixo` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`cd_vendedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_vendedor`
--

/*!40000 ALTER TABLE `tb_vendedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_vendedor` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
