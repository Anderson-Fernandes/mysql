create database db_programacao;

use db_programacao;

create database db_softhause;

use db_softhause;

create table tb_setor
(
  cd_setor int not null,
  nm_setor varchar (45),
  constraint pk_setor
    primary key(cd_setor)
);

show tables;
desc tb_setor;

create table tb_nivel_programador
(
  cd_nivel_programador int not null,
  nm_nivel_programador varchar(45),
  constraint pk_nivel_programador
    primary key(cd_nivel_programador)
);

show tables;
desc tb_nivel_programador;

create table tb_dificuldade
(
  cd_dificuldade int not null,
  cd_nivel_dificuldade tinyint,
  nm_dificuldade varchar(45),
  constraint pk_dificuldade
    primary key(cd_dificuldade)
);

show tables;
desc tb_dificuldade;

create table tb_programa

(
  cd_programa int not null,
  cd_dificuldade int,
  nm_programa varchar(45),
  ds_programa text,
  dt_tempo_estimado int,
  constraint pk_programa
    primary key(cd_programa),
  constraint fk_programa_dificuldade
    foreign key(cd_dificuldade)
      references tb_dificuldade(cd_dificuldade)
);

show tables;
desc tb_programa;

create table tb_programador
(
  cd_programador int not null,
  cd_nivel_programador int,
  cd_setor int,
  nm_programador varchar(45),
  constraint pk_programador
    primary key (cd_programador),
  constraint fk_programador_nivel_programador
    foreign key (cd_nivel_programador)
      references tb_nivel_programador(cd_nivel_programador),
  constraint fk_programador_setor
    foreign key (cd_setor)
      references tb_setor(cd_setor)
);

show tables;
desc tb_programador;

create table tb_programador_programa
(
  cd_programador int,
  cd_programa int,
  constraint fk_programador_programa_programador
    foreign key (cd_programador)
      references tb_programador(cd_programador),
  constraint fk_programador_programa_programa
    foreign key (cd_programa)
      references tb_programa(cd_programa)
);

show tables;
desc tb_programador_programa;

show tables;