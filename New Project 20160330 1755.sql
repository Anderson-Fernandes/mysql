-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema db_videoteca
--

CREATE DATABASE IF NOT EXISTS db_videoteca;
USE db_videoteca;

--
-- Definition of table `tb_filme`
--

DROP TABLE IF EXISTS `tb_filme`;
CREATE TABLE `tb_filme` (
  `cd_filme` int(11) NOT NULL AUTO_INCREMENT,
  `nm_filme` varchar(30) NOT NULL,
  `cd_genero` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd_filme`),
  KEY `fk_filme_genero` (`cd_genero`),
  CONSTRAINT `fk_filme_genero` FOREIGN KEY (`cd_genero`) REFERENCES `tb_genero` (`cd_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_filme`
--

/*!40000 ALTER TABLE `tb_filme` DISABLE KEYS */;
INSERT INTO `tb_filme` (`cd_filme`,`nm_filme`,`cd_genero`) VALUES 
 (1,'a',1),
 (2,'a',1),
 (3,'a',1),
 (4,'s',4),
 (5,'aass',4),
 (6,'ss',5),
 (7,'ff',5),
 (8,'gg',5),
 (9,'zz',5);
/*!40000 ALTER TABLE `tb_filme` ENABLE KEYS */;


--
-- Definition of table `tb_genero`
--

DROP TABLE IF EXISTS `tb_genero`;
CREATE TABLE `tb_genero` (
  `cd_genero` int(11) NOT NULL AUTO_INCREMENT,
  `nm_genero` varchar(20) NOT NULL,
  PRIMARY KEY (`cd_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_genero`
--

/*!40000 ALTER TABLE `tb_genero` DISABLE KEYS */;
INSERT INTO `tb_genero` (`cd_genero`,`nm_genero`) VALUES 
 (1,'Terror'),
 (2,'Suspense'),
 (3,'Comedia'),
 (4,'Romance'),
 (5,'Ação');
/*!40000 ALTER TABLE `tb_genero` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
